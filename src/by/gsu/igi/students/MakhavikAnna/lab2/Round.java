package by.gsu.igi.students.MakhavikAnna.lab2;

import java.util.Scanner;

/**
 * Created by Анна Маховик on 07.10.2017.
 */
public class Round {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите радиус окружности: ");
        double radius = in.nextDouble();
        System.out.println("Радиус=" + radius);
        // Периметр
        double pi = 3.1416;
        double perimeter = (radius + radius) * pi;
        System.out.println("Периметр=" + perimeter);
        //Площадь
        double square = (radius * radius) * pi;
        System.out.println("Площадь=" + square);

    }
}
